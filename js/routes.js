define(['app','angular-route'], function (app) {
    'use strict';
    return app.config(['$routeProvider', function ($routeProvider) {
        $routeProvider
            // Home
            .when("/", {templateUrl: "partials/views/index.html", controller: "IndexController",activetab: 'index'})
            .otherwise("/", {templateUrl: "partials/views/index.html", controller: "IndexController",activetab: 'index'})
            // Pages
            .when("/index", {templateUrl: "partials/views/index.html", controller: "IndexController",activetab: 'index'})
    }])

    .config(['$httpProvider', function($httpProvider) {
        $httpProvider.defaults.headers.post['Content-Type'] = 'application/json';
        $httpProvider.defaults.headers.post = {};
    }])

    .config( [
        '$compileProvider',
        function( $compileProvider )
        {
            $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|file|blob|cid):|data:image\//);
            $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|chrome-extension):/);
        }
    ]);

});