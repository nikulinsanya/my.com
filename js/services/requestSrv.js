define(['./module'], function (services) {
    'use strict';
    services.factory('requestSrv', ['$rootScope','$http', '$q','$window', 'constants',
        function ($rootScope, $http, $q, $window, constants) {
            var service = {
                getPlayers:function() {
                    var self = this,
                        defer = $q.defer(),
                        url = 'data/data.json';

                    $http.get(url).
                        success(function (data, status, headers, config) {
                            defer.resolve(data.players);
                        }).
                        error(function (data, status, headers, config) {
                            defer.reject();
                        });
                    return defer.promise;
                }
            }
            return service;
        }
    ])
});