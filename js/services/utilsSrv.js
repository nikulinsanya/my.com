define(['./module'], function (services) {
    'use strict';
    services.factory('utils', ['constants','$rootScope', function (constants, $rootScope) {
        return {
            extendDeep: function (dst) {
                var self = this;
                angular.forEach(arguments, function (obj) {
                    if (obj !== dst) {
                        angular.forEach(obj, function (value, key) {
                            if (dst[key] && dst[key].constructor && dst[key].constructor === Object) {
                                self.extendDeep(dst[key], value);
                            } else {
                                dst[key] = value;
                            }
                        });
                    }
                });
                return dst;
            }
        }
    }])
});