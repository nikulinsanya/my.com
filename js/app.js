define([
    'angular',
    './controllers/index',
    './directives/index',
    './services/index',
], function (ng) {
    'use strict';

    return ng.module('app', [
        'ngRoute',
        'app.controllers',
        'app.services',
        'app.directives'
    ]);
});