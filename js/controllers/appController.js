define(['./module'], function (controllers) {
    'use strict';
    controllers.controller('AppController', ['$scope',
        function ($scope) {

        var self = $scope.model = {
            user:{
                username: "Ingeborge_Dopkunayte"
            },
            joinCBT: function(){
                self.user.joined = true;
            }
        }
    }]);
});