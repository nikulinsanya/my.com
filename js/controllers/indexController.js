define(['./module'], function (controllers) {
    'use strict';
    controllers.controller('IndexController', ['$scope', 'constants','utils','requestSrv',
        function ($scope,  constants, utils, requestSrv) {

        var self = $scope.model = {
            current:0,
            players:[],
            player:{},
            next: function(){
                if(self.current >= self.players.length-1){
                    return false;
                }
                self.player  = self.players[++self.current];
            },
            prev: function(){
                if(!self.current){
                    return false;
                }

                self.player  = self.players[--self.current];
            },
            vote: function(){
                if(!this.player.voted) {
                    this.player.points++;
                    this.player.voted = true;
                }
                //and send an update to a server
            }
        }

        init();

        function init(){
            requestSrv.getPlayers().then(function(players){
                self.players = players;
                self.player  = self.players[self.current];
            })
        }

    }]);
});