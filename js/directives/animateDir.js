define(['./module'], function (directives) {
    'use strict';
    directives.directive('animate', ['$animate', '$timeout',
        function ($animate, $timeout) {
            return {
                scope: {
                    count: '='
                },
                link: function (scope, element, attrs) {

                    scope.$watch('count', function (newValue, oldValue) {
                        if (newValue.name !== oldValue.name ||
                            newValue.points === oldValue.points
                        ){
                            return;
                        }
                        animate();
                    },true);

                    var animate = function () {
                        $animate.addClass(element, 'animate').then(function () {
                            $timeout(function(){
                                element.removeClass('animate');
                            },1000);

                        });
                    };
                }
            };
        }
    ]);
});