require.config({
    paths: {
        'angular': 'lib/angular/angular',
        'domReady': 'lib/angular/domReady',
        'angular-route': 'lib/angular/angular-route.min',
        'jquery': 'lib/jquery/jquery.min'
    },

    shim: {
        'angular': {
            exports: 'angular',
            deps:['jquery']
        },
        'angular-route': {
            deps:['angular']
        },
        'jquery':{
            exports: 'jquery'
        }
    },
    deps: [
        './bootstrap',
        'jquery'
    ]
});